package network

import (
	"fmt"
	"net/http"
	"time"
)

// Client used to send requests
type Client struct {
	// Headers to include on requests
	// every one is optional
	Headers map[string]*string

	httpClient http.Client
}

var (
	// COOKIE stored to prevent typos being annoying
	COOKIE = "Cookie"
	// XSRFTOKEN stored to prevent typos being annoying
	XSRFTOKEN = "X-CSRF-TOKEN"
)

// NewClient create new client
func NewClient(cookie *string, xsrf *string) *Client {
	client := &Client{
		Headers: map[string]*string{
			COOKIE:    cookie,
			XSRFTOKEN: xsrf,
		},
		httpClient: http.Client{
			Timeout: 5 * time.Second,
		},
	}
	return client
}

// With specify the headers the request should have
func (c *Client) With(p ...string) *Request {
	final := make(map[string]string)
	for _, h := range p {
		val, has := c.Headers[h]
		if !has {
			fmt.Printf("Network Client header %s does not exist", h)
			continue
		}
		final[h] = *val

	}
	request := &Request{
		Headers:    final,
		httpClient: c.httpClient,
	}
	return request
}

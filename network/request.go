package network

import (
	"fmt"
	"io"
	"net/http"
)

// Request struct to store the request the client is getting ready to do
type Request struct {
	// Headers to use on the request
	Headers map[string]string

	// URL page to request
	URL string

	httpClient http.Client
}

// Response the request gives
type Response struct {
	Body       io.ReadCloser
	StatusCode int
	Error      error
}

// SetURL method to set the url
func (r *Request) SetURL(url string) *Request {
	r.URL = url
	return r
}

// Get request the page with the current params
func (r *Request) Get() Response {
	req, _ := http.NewRequest("GET", r.URL, nil)
	for k, v := range r.Headers {
		if k == "Cookie" {
			v = fmt.Sprintf(".ROBLOSECURITY=%s", v)
		}
		req.Header.Add(k, v)
	}
	res, err := r.httpClient.Do(req)
	if err != nil {
		return Response{Error: err}
	}

	return Response{
		Body:       res.Body,
		StatusCode: res.StatusCode,
	}
}
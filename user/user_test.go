package user

import (
	"rbxgo/endpoints"
	"testing"
)

func TestEndpointGeneration(t *testing.T) {
	want := "https://auth.roblox.com/"
	if got := endpoints.GetAPIUrl("auth"); got != want {
		t.Errorf("GetAPIUrl() = %q, want %q", got, want)
	}
}

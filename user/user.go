package user

import (
	"encoding/json"
	"rbxgo/network"
)

// User main struct to store the user
type User struct {
	// ID of user
	// Retrieved on login or when necessary
	ID int

	// Username of user
	// Retrieved on login and will not refresh unless specified
	Username string

	// User's XSRF_TOKEN
	// Changes every 30 minutes
	XSRF string

	// Whether the user is currently authenticated
	LoggedIn bool

	// HTTP client for the user
	// Used for easier access to
	Client *network.Client

	cookie string
}

// Login logs the user in with a cookie
func Login(cookie string) (*User, error) {
	user := &User{
		cookie: cookie,
	}
	user.Client = network.NewClient(&user.cookie, &user.XSRF)
	id, u, err := user.verifyLogin()
	if err != nil {
		return nil, err
	}
	user.ID, user.Username = id, u
	return user, nil
}

func (user *User) verifyLogin() (int, string, error) {
	api := user.Client.With(network.COOKIE).SetURL("https://www.roblox.com/mobileapi/userinfo").Get()
	if api.Error != nil {
		return 0, "", api.Error
	}

	defer api.Body.Close()
	
	var data struct {
		UserID int
		UserName string
	}
	json.NewDecoder(api.Body).Decode(&data)
	return data.UserID, data.UserName, nil
}

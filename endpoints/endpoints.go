package endpoints

var (
	// GetAPIUrl takes subdomain name and returns it with all url info
	GetAPIUrl = func(sub string) string { return "https://" + sub + ".roblox.com/" }

	authSubdomain      = GetAPIUrl("auth") + "v2/"
	authLoginEndpoint  = authSubdomain + "/login"
	authLogoutEndpoint = authSubdomain + "/logout"
)

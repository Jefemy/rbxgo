package endpoints

import "testing"

func TestEndpointGeneration(t *testing.T) {
	want := "https://auth.roblox.com/"
	if got := GetAPIUrl("auth"); got != want {
		t.Errorf("GetAPIUrl() = %q, want %q", got, want)
	}
}
